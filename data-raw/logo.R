## code to prepare `logo` dataset goes here
library(hexSticker)
library(showtext)
font_add_google("Eagle Lake")

library(ggplot2)

p <- ggplot(aes(x = mpg, y = wt), data = mtcars) + geom_point()
p <- p + theme_void() + theme_transparent()
img <- "inst/figures/database.png"
sticker(img, package="datalibaba",
        s_width = 0.3,
        s_height  = 0.3,
        p_family = "Eagle Lake",
        p_size=20, s_x=1, s_y=.75,
        filename="inst/figures/logo.png",
        dpi = 400,
        h_fill = "#E2B835",
        h_color = "#FDCF41")
