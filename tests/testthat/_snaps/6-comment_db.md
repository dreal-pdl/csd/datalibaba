# get_db_comment works

    Code
      get_db_comment(db = "test_datalibaba", user = "test", server = Sys.getenv(
        "server"))
    Output
      [1] nom_base    commentaire
      <0 rows> (or 0-length row.names)

# get_schema_comment  works

    Code
      get_schema_comment(schema = "public", db = "test_datalibaba", user = "test",
        server = Sys.getenv("server"))
    Output
        nom_schema                       commentaire
      1     public le schema des tests qui restent !

