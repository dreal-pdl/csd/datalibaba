# get_cols_comment works

    Code
      get_cols_comment(table = "test_iris", schema = "public", db = "test_datalibaba",
        user = "test", server = Sys.getenv("server"))
    Output
        nom_schema nom_table nom_col     commentaire
      1     public test_iris species Commentlibaba !

# get_table_comments works

    Code
      get_table_comments(table = "retest_iris", schema = "public", db = "test_datalibaba",
        user = "test", server = Sys.getenv("server"))
    Output
        nom_schema   nom_table                 commentaire      nom_col
      1     public retest_iris                        <NA>         <NA>
      2     public retest_iris libellé de Sepal.Length  cc Sepal.Length
      3     public retest_iris  libellé de Sepal.Width  cc  Sepal.Width
      4     public retest_iris libellé de Petal.Length  cc Petal.Length
      5     public retest_iris  libellé de Petal.Width  cc  Petal.Width
      6     public retest_iris      libellé de Species  cc      Species

# get_table_comment works

    Code
      get_table_comment(table = "retest_iris", schema = "public", db = "test_datalibaba",
        user = "test", server = Sys.getenv("server"))
    Output
        nom_schema   nom_table commentaire
      1     public retest_iris        <NA>

