library(DBI)
library(magrittr)
library(tibble)
library(lubridate)
library(dplyr)
library(sf)
library(glue)
library(backports)
library(withr)

test_that("importer_data no geo works", {
  if(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres")){
    Sys.setenv(port="5432")
    Sys.setenv(server="localhost")
    Sys.setenv(user_test="runner")
    Sys.setenv(pwd_test=Sys.getenv("BDD_RUNNER_PWD"))

    expect_error(importer_data(schema = "public", db = "test_datalibaba", user = "test"),
                 "l'argument table n'est pas renseign\u00e9")
    expect_error(importer_data(table = "test_iris", db = "test_datalibaba", user = "test"),
                 "l'argument schema n'est pas renseign\u00e9")
    expect_error(importer_data(table = "test_iris", schema = "public", db = NULL, user = "test"),
                 "l'argument db n'est pas renseign\u00e9")
    expect_error(importer_data(table = "test_iris", schema = "public", db = "test_datalibaba", user = NULL),
                 "l'argument user n'est pas renseign\u00e9")
    expect_error(importer_data(table = "test_iris", schema = "public", db = "test_datalibaba", user = "test", limit="d"),
                 "l'argument limit n'est pas num\u00e9rique")

    expect_error(importer_data(table = "titi-test", schema = "public", db = "test_datalibaba", user = "test"),
                 paste0("La table 'public'.'titi-test' est introuvable."))
    # test avec création schema sans table de propriétés
    schem <- paste0("test_",sample.int(1000, 1),"_",sample.int(1000, 1))
    suppressWarnings(poster_data(data = iris, table = "test_iris", schema = schem, db = "test_datalibaba", pk = NULL, post_row_name = FALSE, overwrite = TRUE, user = "test"))
    condirect <- DBI::dbConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="test_datalibaba")
    DBI::dbExecute(condirect, paste0("DELETE FROM ",schem,".zz_r_df_def;"))
    expect_message(importer_data(table = "test_iris", schema = schem, db = "test_datalibaba", user = "test"),
                   paste0("Pas de renseignements trouv\u00e9 sur test_iris dans la table des propri\u00e9t\u00e9s des dataframes R, un import standard sera r\u00e9alis\u00e9. "))

    DBI::dbExecute(condirect, paste0("DROP TABLE IF EXISTS ",schem,".zz_r_df_def CASCADE;"))
    expect_message(importer_data(table = "test_iris", schema = schem, db = "test_datalibaba", user = "test"),
        "La table de propri\u00e9t\u00e9s des dataframes R est introuvable, un import standard sera r\u00e9alis\u00e9. ")
    # test basic
    expect_snapshot(importer_data(table = "test_iris", schema = "public", db = "test_datalibaba", user = "test", limit = 4, ecoSQL = FALSE))

  }else{
    print(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres"))
    print('warning: connect_to_db - local database postgresql doesnt work !')
    expect_equal(1,1)
  }
})

test_that("importer_data geo works", {
  withr::local_options(lifecycle_verbosity = "quiet")
  if(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres")){
    Sys.setenv(port="5432")
    Sys.setenv(server="localhost")
    Sys.setenv(user_test="runner")
    Sys.setenv(pwd_test=Sys.getenv("BDD_RUNNER_PWD"))

    # test geometry # pas pris en compte dans le coverage... pourquoi ??
    expect_snapshot(importer_data(table = "nc", schema = "public", db = "test_datalibaba", user = "test"))
    expect_snapshot(importer_data(table = "nc", schema = "public", db = "test_datalibaba", user = "test", limit = 4))
    nc <- st_read(system.file("shape/nc.shp", package="sf"))
    poster_data(data = nc, table = "test_nc_noid", schema = "public", db = "test_datalibaba", pk = "CNTY_ID", post_row_name = FALSE, overwrite = TRUE, user = "test")
    expect_snapshot(importer_data(table = "test_nc_noid", schema = "public", db = "test_datalibaba", user = "test", limit = 4))
    #A FINIR !!!

  }else{
    print(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres"))
    print('warning: connect_to_db - local database postgresql doesnt work !')
    expect_equal(1,1)
  }
})

test_that("get_data works", {
  if(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres")){
    Sys.setenv(port="5432")
    Sys.setenv(server="localhost")
    Sys.setenv(user_test="runner")
    Sys.setenv(pwd_test=Sys.getenv("BDD_RUNNER_PWD"))

    expect_error(get_data(con=NULL, schema = "public", table = "test_iris"),
                 "con n\'est pas renseign\u00e9")
    con<<-connect_to_db(db = 'test_datalibaba', user = 'test')
    expect_error(get_data(con=con, schema = "public", table = "test_iris"),
                 "veuillez utiliser une connexion avec ecoSQL=FALSE")
    con<<-connect_to_db(db = 'test_datalibaba', user = 'test', ecoSQL = FALSE)
    expect_error(get_data(con=con, table = "test_iris"),
                 "schema n\'est pas renseign\u00e9")
    expect_error(get_data(con=con, schema = "public"),
                 "table n\'est pas renseign\u00e9")
    expect_snapshot(get_data(con=con, schema = "public", table = "test_iris"))

  }else{
    print(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres"))
    print('warning: connect_to_db - local database postgresql doesnt work !')
    expect_equal(1,1)
  }
})

test_that("get_data_dbi works", {
  if(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres")){
    Sys.setenv(port="5432")
    Sys.setenv(server="localhost")
    Sys.setenv(user_test="runner")
    Sys.setenv(pwd_test=Sys.getenv("BDD_RUNNER_PWD"))

    expect_error(get_data_dbi(con=NULL, schema = "public", table = "test_iris"),
                 "con n\'est pas renseign\u00e9")
    con<<-connect_to_db(db = 'test_datalibaba', user = 'test', ecoSQL = FALSE)
    expect_error(get_data_dbi(con=con, schema = "public", table = "test_iris"),
                 "veuillez utiliser une connexion avec ecoSQL=TRUE")
    con<<-connect_to_db(db = 'test_datalibaba', user = 'test')
    expect_error(get_data_dbi(con=con, table = "test_iris"),
                 "schema n\'est pas renseign\u00e9")
    expect_error(get_data_dbi(con=con, schema = "public"),
                 "table n\'est pas renseign\u00e9")
    expect_snapshot(get_data_dbi(con=con, schema = "public", table = "test_iris"))

  }else{
    print(DBI::dbCanConnect(RPostgres::Postgres(), host="localhost", user="runner", password=Sys.getenv("BDD_RUNNER_PWD"), dbname="postgres"))
    print('warning: connect_to_db - local database postgresql doesnt work !')
    expect_equal(1,1)
  }
})
